#!/bin/bash
#
# [Quick LEMP Stack Installation Script]
#
# GitLab:   https://gitlab.com/devops212/lempqc
# Modief by   wiros4bleng
#
bold=$(tput bold)
normal=$(tput sgr0)
cat <<!

${bold}[quick-lemp] Stack Installation${normal}

Konfigurasi dan testing untuk Ubuntu 12.04, 13.04, 14.04, 15.04, 16.04dan 18.04.
Instalasi Nginx, MariaDB, PHP-FPM, dan deploy sampel
halaman phpinfo() untuk tes konfigurasi.
Info lebih lanjut ${bold}https://blog.mastohir.com${normal}

!
read -p "${bold}Do you want to continue?[y/N]${normal} " -n 1 -r
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
  echo 'Exiting...'
  exit 1
fi
echo
echo
echo 'Checking distribution ...'
if [ ! -x  /usr/bin/lsb_release ]; then
  echo 'You do not appear to be running Ubuntu.'
  echo 'Exiting...'
  exit 1
fi
echo "$(lsb_release -a)"
echo
dis="$(lsb_release -is)"
rel="$(lsb_release -rs)"
if [[ "${dis}" != "Ubuntu" ]]; then
  echo "${dis}: You do not appear to be running Ubuntu"
  echo 'Exiting...'
  exit 1
elif [[ ! "${rel}" =~ ("12.04"|"13.04"|"14.04"|"15.04"|"16.04"|"18.04") ]]; then #
  echo "${bold}${rel}:${normal} You do not appear to be running a supported Ubuntu release."
  echo 'Exiting...'
  exit 1
fi
echo 'Checking permissions...'
echo
if [[ $EUID -ne 0 ]]; then
  echo 'This script must be run with root privileges.' 1>&2
  echo 'Exiting...'
  exit 1
fi

# locale set
echo 'en_US.UTF-8 UTF-8' >>  /etc/locale.gen
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
/usr/sbin/locale-gen

# Update packages and add MariaDB repository
echo -e '\n[Package Updates]'
apt-get install software-properties-common
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
add-apt-repository "deb [arch=amd64,i386] http://nyc2.mirrors.digitalocean.com/mariadb/repo/10.3/ubuntu $(lsb_release -sc) main"
#add-apt-repository ppa:ondrej/nginx-mainline
add-apt-repository ppa:nginx/stable
add-apt-repository ppa:ondrej/php
apt-get update
apt-get -y upgrade

# Nginx
echo -e '\n[Nginx]'
apt-get -y install nginx
service nginx stop
curl -L https://gitlab.com/tohir212/instan-install-lemp/raw/master/server-configs-nginx-latest.tar.gz | tar -xz
rm /etc/nginx/sites-available/*
rm /etc/nginx/sites-enabled/*
mv server-configs-nginx-latest/sites-available/* /etc/nginx/sites-available
mv server-configs-nginx-latest/nginx.conf /etc/nginx
cp server-configs-nginx-latest/fastcgi_params /etc/nginx

echo
read -p 'Do you want to create a self-signed SSL cert and configure HTTPS? [y/N] ' -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]; then
  conf1="  listen [::]:443 ssl default_server;\n  listen 443 ssl default_server;\n"
  conf2="  include h5bp/directive-only/ssl.conf;\n  ssl_certificate /etc/ssl/certs/nginx.crt;\n  ssl_certificate_key /etc/ssl/private/nginx.key;"
  openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx.key -out /etc/ssl/certs/nginx.crt
  chmod 400 /etc/ssl/private/nginx.key
else
  conf1=
  conf2=
  conf3=
fi

ln -s /etc/nginx/sites-available/example.com /etc/nginx/sites-enabled/example.com

# PHP 5.6
echo -e '\n install php5.6?[y/n]'
apt-get install php5.6-common php5.6-mysql php5.6-curl php5.6-gd php5.6-cli php5.6-fpm php-pear php5.6-dev php5.6-imap php5.6-mcrypt php5.6-imagick php5.6-zip php5.6-bz2 php5.6-soap php5.6-cgi php5.6-mbstring php5.6-xml
# PHP 7.1
echo -e '\n install php7.1?[y/n]'
apt-get install php7.1-common php7.1-mysql php7.1-curl php7.1-gd php7.1-cli php7.1-fpm php-pear php7.1-dev php7.1-imap php7.1-mcrypt php7.1-imagick php7.1-zip php7.1-bz2 php7.1-soap php7.1-cgi php7.1-mbstring php7.1-xml
echo '<?php phpinfo(); ?>' > /var/www/html/checkinfo.php

# Create Users
# Users
echo -e '\n[Create New User, set passwd]'
echo -e 'If this field is left blank, the default account listed will be updated.'
read -p "Username[$SUDO_USER]: " -r newuser
if [ -z "$newuser" ]; then
  newuser=$SUDO_USER
fi
egrep "^$newuser" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
  echo "$newuser exists, skipping ahead..."
else
  unset newpass;
  echo -n 'Password: '
  while IFS= read -r -s -n 1 newchar; do
    if [[ -z $newchar ]]; then
       echo
       break
    else
       echo -n '*'
       newpass+=$newchar
    fi
  done
  useradd $newuser -s /bin/bash -m
  echo "$newuser:$newpass" | chpasswd
fi
usermod -a -G sudo $newuser
usermod -a -G www-data $newuser
usermod -a -G www-data $whoami

# Permissions
echo -e '\n[Adjusting Permissions]'
chown -R $whoami:www-data /var/www/html
sh -c 'find /var/www/html -type d -print0 | xargs -0 chmod 0750'
sh -c 'find /var/www/html -type d -print0 | xargs -0 chmod g+s'
sh -c 'find /var/www/html -type f -print0 | xargs -0 chmod 0640'
chown -R www-data:root /var/lib/nginx
chown -R www-data:root /var/lib/php
chmod 755 /var/lib/php/sessions
chown -R www-data:root /var/run/php
chown -R www-data:root /usr/lib/nginx/modules
chown -R www-data:root /usr/lib/php/5.6

# Config pool
sed -i 's|pm.max_children = 5|pm.max_children = 100|g' /etc/php/5.6/fpm/pool.d/www.conf
sed -i 's|pm.start_servers = 2|pm.start_servers = 5|g' /etc/php/5.6/fpm/pool.d/www.conf
sed -i 's|pm.min_spare_servers = 1|pm.min_spare_servers = 3|g' /etc/php/5.6/fpm/pool.d/www.conf
sed -i 's|pm.max_spare_servers = 3|pm.max_spare_servers = 10|g' /etc/php/5.6/fpm/pool.d/www.conf
sed -i 's|;pm.max_requests = 500|pm.max_requests = 200|g' /etc/php/5.6/fpm/pool.d/www.conf
sed -i 's|pm.max_children = 5|pm.max_children = 100|g' /etc/php/7.1/fpm/pool.d/www.conf
sed -i 's|pm.start_servers = 2|pm.start_servers = 5|g' /etc/php/7.1/fpm/pool.d/www.conf
sed -i 's|pm.min_spare_servers = 1|pm.min_spare_servers = 3|g' /etc/php/7.1/fpm/pool.d/www.conf
sed -i 's|pm.max_spare_servers = 3|pm.max_spare_servers = 10|g' /etc/php/7.1/fpm/pool.d/www.conf
sed -i 's|;pm.max_requests = 500|pm.max_requests = 200|g' /etc/php/7.1/fpm/pool.d/www.conf

# Config php.ini
chown -R www-data:root /var/lib/nginx && chmod g+s /var/lib/nginx
chown -R www-data:root /var/lib/php && chmod g+s /var/lib/php
chmod 755 /var/lib/php/sessions
chown -R www-data:root /var/run/php/ && chmod g+s /var/run/php
chown -R www-data:root /usr/lib/nginx/modules && chmod g+s /usr/lib/nginx/modules
chown -R www-data:root /usr/lib/php/5.6 && chmod g+s /usr/lib/php
chown -R www-data:www-data /usr/lib/cgi-bin && chmod g+s /usr/lib/cgi-bin && chmod u+s /usr/lib/cgi-bin
chown -R www-data:www-data /usr/lib/php && chmod g+s /usr/lib/php && chmod u+s /usr/lib/php
chown -R www-data:www-data /usr/lib/nginx && chmod g+s /usr/lib/nginx && chmod u+s /usr/lib/nginx
sed -i 's|upload_max_filesize = 2M|upload_max_filesize = 32M|g' /etc/php/5.6/fpm/php.ini

# MariaDB
echo -e '\n[MariaDB]'
export DEBIAN_FRONTEND=noninteractive
apt-get -y install mariadb-server
curl -LO https://gitlab.com/tohir212/instan-install-lemp/raw/master/mysql/my.cnf
mv my.cnf /etc/mysql/

# Start
service nginx restart
service php5.6-fpm restart
service php7.1-fpm restart
service mysql restart

echo
read -p 'Do you want to install phpmyadmin git version? [y/N] ' -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]; then
  cd /var/www/html && git clone -b STABLE https://github.com/phpmyadmin/phpmyadmin.git
  echo 'Congrat! Installation completed..'
else
  echo 'Congrat! Installation completed..'
fi

exit 0
