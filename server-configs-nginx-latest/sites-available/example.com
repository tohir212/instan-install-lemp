upstream php5.6-fpm{
  server  unix:/run/php/php5.6-fpm.sock;
  }

upstream php7.1-fpm{
  server  unix:/run/php/php7.1-fpm.sock;
  }

server {
  listen 80;
  server_name  localhost example.com;
  root   /var/www/html;
  index  index.php index.html;
  #autoindex on;

  #limit_req zone=one burst=40;

  #HTTP secure Header

  # X-Frame-Options is to prevent from clickJacking attack

  #  disable content-type sniffing on some browsers.
  add_header X-Content-Type-Options nosniff;

  # This header enables the Cross-site scripting (XSS) filter
  add_header X-XSS-Protection "1; mode=block";

  # This will enforce HTTP browsing into HTTPS and avoid ssl stripping attack
  add_header Strict-Transport-Security "max-age=31536000; includeSubdomains;";

  #Content Security Policy

  #Public-Key-Pins
  #add_header Public-Key-Pins 'pin-sha256="klO23nT2ehFDXCfx3eHTDRESMz3asj1muO+4aIdjiuY="; pin-sha256="633lt352PKRXbOwf4xSEa1M517scpD3l5f79xMD9r9Q="; max-age=2592000; includeSubDomains';
  add_header Public-Key-Pins 'pin-sha256="YLh1dUR9y6Kja30RrAn7JKnbQG/uEtLMkBgFF2Fuihg="; pin-sha256="sRHdihwgkaib1P1gxX8HFszlD+7/gTfNvuAybgLPNis="; max-age=5184000; includeSubDomains';

  #Referrer-Policy
  add_header 'Referrer-Policy' 'origin-when-cross-origin';
  #The value 'strict-origin-when-cross-origin' is not one of 'no-referrer', 'no-referrer-when-downgrade', 'origin', 'origin-when-cross-origin', or 'unsafe-url'. The referrer policy has been left unchanged.

  #Limit HTTP Method

  ## Only GET, Post, PUT are allowed##
  if ($request_method !~ ^(GET|PUT|POST)$ ) {
    return 444;
  }
  ## In this case, it does not accept other HTTP method such as HEAD, DELETE, SEARCH, TRACE ##

  location / {
  try_files $uri $uri/ /index.php?$uri&$args;
  }

  #Dont serve dot/hidden files, except .well-known

  location ~ /\. {
    deny all;
    access_log off;
    log_not_found off;
    return 404;
  }

  location ^~ /.well-known/ {
    allow all;
  }

  location ~* \.(jpg|jpeg|gif|png|css|js|ico|xml)$ {
    access_log off;
    log_not_found off;
    expires 360d;
  }

  error_page 403 404 /img/custom_404.html;
  location = /img/custom_404.html {
    root /var/www/html;
    internal;
  }

  error_page 500 502 503 504 /img/custom_50x.html;
    location = /img/custom_50x.html {
    root /var/www/html;
    internal;
  }

  location ~ [^/]\.php(/|$) {
    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
    if (!-f $document_root$fastcgi_script_name) {
      return 404;
    }

    # Mitigate https://httpoxy.org/ vulnerabilities
    fastcgi_param HTTP_PROXY "";

    fastcgi_pass php5.6-fpm;
    #fastcgi_pass php7.1-fpm;
    fastcgi_index index.php;
    fastcgi_read_timeout 600;
    include fastcgi_params;
  }

  # Start codeigniter
  location /ci {
		try_files $uri $uri/ /ci/index.php?$uri&$args;
	}
  # End codeigniter
  
  # Start lareavel 5
  location ^~ /laravel {
    alias /var/www/html/laravel/public;
    try_files $uri $uri/ @laravel;
    location ~ \.php {
      include snippets/fastcgi-php.conf;
      fastcgi_pass unix:/run/php/php7.1-fpm.sock;
      fastcgi_param SCRIPT_FILENAME /var/www/html/laravel/public/index.php;
    }
  }

  location @laravel {
    rewrite /laravel/(.*)$ /laravel/index.php?/$1 last;
  }
  # End lareavel 5
}
