server {
	#listen	443 ssl;
	listen 80;
	server_name	laravel.example.com localhost;
	root	/var/www/html/laravel/public;
	index	index.php;

	#### HTTP secure Header ####

	# X-Frame-Options is to prevent from clickJacking attack
	add_header X-Frame-Options SAMEORIGIN;

	# disable content-type sniffing on some browsers.
	add_header X-Content-Type-Options nosniff;

	# This header enables the Cross-site scripting (XSS) filter
	add_header X-XSS-Protection "1; mode=block";

	# This will enforce HTTP browsing into HTTPS and avoid ssl stripping attack
	add_header Strict-Transport-Security "max-age=31536000; includeSubdomains;";

	# Referrer-Policy
	# add_header 'Referrer-Policy' 'origin-when-cross-origin';
	add_header 'Referrer-Policy' 'no-referrer';

	# Expect-CT
	add_header Expect-CT 'enforce; max-age=2764800';

	#Limit HTTP Method
	## Only GET, Post, PUT are allowed##
	if ($request_method !~ ^(GET|PUT|POST)$ ) {
		return 444;
	}

	#Allow Access To Specified Domain Only
	#if ($host !~ ^(laravel.example.com)$ ) {
	#	return 444;
	#}

	#Dont serve dot/hidden files, except .well-known
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
		return 404;
	}

	# We don't need .ht files with nginx.
	location ~ /\.ht {
	deny all;
	}

	# Set header expirations on per-project basis
	location ~* \.(ico|css|js|gif|jpeg|jpg|png|woff|ttf|otf|svg|woff2|eot|webp)$ {
		expires 30d;
		add_header Pragma public;
		add_header Cache-Control "public";
		add_header Access-Control-Allow-Origin *;
	}

	location ^~ /.well-known/ {
		allow all;
	}

	location / {
	# URLs to attempt, including pretty ones.
	try_files   $uri $uri/ /index.php?$query_string;

	}

	# Remove trailing slash to please routing system.
	if (!-d $request_filename) {
	rewrite     ^/(.+)/$ /$1 permanent;
	}

	# PHP FPM configuration.
	location ~* \.php$ {

	fastcgi_pass                    unix:/run/php/php7.1-fpm.sock;
	fastcgi_index                   index.php;
	fastcgi_split_path_info         ^(.+\.php)(.*)$;
	include                         /etc/nginx/fastcgi_params;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 64k;
        fastcgi_buffers 256 64k;
	}

	location = /favicon.ico { access_log off; log_not_found off; }
	location = /robots.txt  { access_log off; log_not_found off; }

	access_log off;
}

#server {
#	listen 80;
#	server_name laravel.example.com;
#	return 301 https://$host$request_uri;
#}
