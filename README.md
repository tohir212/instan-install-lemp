# Automation install LEMP (Linux Nginx MariaDB PHP) kompatible untuk Ubuntu 12.04, 13.04, 14.04, 15.04, 16.04, 18.04 Desktop/Servers.

Komponen yang akan di-install:
- NGINX Latest Stable
- PHP 5.6 dan PHP 7.1 (sudah termasuk FPM)
- Mariadb 10.3.x

Jika ada software terkait yang terinstall di PC Server/Desktop, terlebih dahulu:

```
apt-get remove --purge --auto-remove apache2* mysql* nginx* mariadb-* php*
apt-get autoclean
apt-get autoremove
```

### Cara Instalasi: ###

```
curl -LO https://gitlab.com/tohir212/instan-install-lemp/raw/master/automate-install-lemp.sh
chmod +x automate-install-lemp.sh
./automate-install-lemp.sh
```
Baca dengan teliti, dan ikuti langkah-langkahnya.

Edit di /etc/nginx/sites-available/example.com dan ssl.example.com utk memilih menggunakan php5.6-fpm atau php7.1-fpm
